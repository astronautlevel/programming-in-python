from random import randint
import pygame,sys
from pygame.locals import *
comhealth = 100
playerhealth = 100
attack = 1
pygame.init()
windowSurface = pygame.display.set_mode((768,432),0,32)
pygame.display.set_caption("Turn Based Battle")
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
basicFont = pygame.font.SysFont(None, 22)
secondFont = pygame.font.SysFont(None, 20)
damageFont = pygame.font.SysFont(None, 45)
pointerlocation = (530,361)
pointer = pygame.image.load('Pointer.png')
background = pygame.image.load('Battlefield.png')
winscreen = pygame.image.load('Win screen.png')
losescreen = pygame.image.load('Lose screen.png')
playerone = pygame.image.load('Player1.png')
playeronefront = pygame.image.load('Player1front.png')
playertwo = pygame.image.load('Player2.png')
playertwofront = pygame.image.load('Player2front.png')
playerthree = pygame.image.load('Player3.png')
playerthreefront = pygame.image.load('Player3front.png')
def pmain():
    global comhealth
    global RED
    global damageFont
    global playerhealth
    damage = randint(18,25)
    comhealth -= damage
    print "Player uses it's main attack for", damage, "damage!"
    if comhealth <= 0:
        while True:
            windowSurface.blit(winscreen, (0,0))
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
    comhealthtext = basicFont.render(str(comhealth), True, BLACK)
    playerhealthtext = secondFont.render(str(playerhealth), True, BLACK)
    windowSurface.blit(background, (0,0))
    windowSurface.blit(comhealthtext, (555,89))
    damagetext = damageFont.render(str(damage), True, RED)
    windowSurface.blit(damagetext, (415,180))
    windowSurface.blit(playerhealthtext, (140,160))
    global player
    windowSurface.blit(player, (81,229))
    pygame.display.update()
    pygame.time.delay(1000)
    comattack()

def pspec():
    global comhealth
    global RED
    global damageFont
    global playerhealth
    damage = randint(13,35)
    if randint(1,10) <= 8:
        comhealth -= damage
        print "Player uses it's special attack for", damage, "damage!"
        if comhealth <= 0:
            while True:
                windowSurface.blit(winscreen, (0,0))
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()
        damagetext = damageFont.render(str(damage), True, RED)
    else:
        damagetext = damageFont.render("Miss", True, RED)
    comhealthtext = basicFont.render(str(comhealth), True, BLACK)
    playerhealthtext = secondFont.render(str(playerhealth), True, BLACK)
    windowSurface.blit(background, (0,0))
    windowSurface.blit(comhealthtext, (555,89))
    windowSurface.blit(damagetext, (415,180))
    windowSurface.blit(playerhealthtext, (140,160))
    global player
    windowSurface.blit(player, (81,229))
    pygame.display.update()
    pygame.time.delay(1000)
    comattack()
    
def pheal():
    global playerhealth
    global comhealth
    global GREEN
    global damageFont
    heal = randint(18,25)
    playerhealth += heal
    print "Player heals", heal, "health!"
    if playerhealth > 100:
        playerhealth = 100
    comhealthtext = basicFont.render(str(comhealth), True, BLACK)
    windowSurface.blit(comhealthtext, (555,89))
    comhealthtext = basicFont.render(str(comhealth), True, BLACK)
    playerhealthtext = secondFont.render(str(playerhealth), True, BLACK)
    windowSurface.blit(background, (0,0))
    windowSurface.blit(comhealthtext, (555,89))
    damagetext = damageFont.render(str(heal), True, GREEN)
    global player
    windowSurface.blit(player, (81,229))
    windowSurface.blit(damagetext, (305,250))
    windowSurface.blit(playerhealthtext, (140,160))
    pygame.display.update()
    pygame.time.delay(1000)
    comattack()
def cmain():
    global playerhealth
    damage = randint(18,25)
    playerhealth -= damage
    print "Computer uses it's main attack for", damage, "damage!"
    if playerhealth <= 0:
            while True:
                windowSurface.blit(losescreen, (0,0))
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()
    comhealthtext = basicFont.render(str(comhealth), True, BLACK)
    playerhealthtext = secondFont.render(str(playerhealth), True, BLACK)
    windowSurface.blit(background, (0,0))
    windowSurface.blit(comhealthtext, (555,89))
    damagetext = damageFont.render(str(damage), True, RED)
    global player
    windowSurface.blit(player, (81,229))
    windowSurface.blit(damagetext, (305,250))
    windowSurface.blit(playerhealthtext, (140,160))
    pygame.display.update()
    pygame.time.delay(1000)
    main()

def cspec():
    global playerhealth
    damage = randint(13,35)
    if randint(1,10) <= 8:
        
        playerhealth -= damage
        print "Computer uses it's special attack for", damage, "damage!"
        if playerhealth <= 0:
            while True:
                windowSurface.blit(losescreen, (0,0))
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()
        damagetext = damageFont.render(str(damage), True, RED)
    else:
        damagetext = damageFont.render("Miss", True, RED)
    comhealthtext = basicFont.render(str(comhealth), True, BLACK)
    playerhealthtext = secondFont.render(str(playerhealth), True, BLACK)
    windowSurface.blit(background, (0,0))
    windowSurface.blit(comhealthtext, (555,89))
    global player
    windowSurface.blit(player, (81,229))
    windowSurface.blit(damagetext, (305,250))
    windowSurface.blit(playerhealthtext, (140,160))
    pygame.display.update()
    pygame.time.delay(1000)
    main()

def cheal():
    global comhealth
    heal = randint(18,25)
    comhealth += heal
    print "Computer heals", heal, "health!"
    if comhealth > 100:
        comhealth = 100
    comhealthtext = basicFont.render(str(comhealth), True, BLACK)
    windowSurface.blit(comhealthtext, (555,89))
    comhealthtext = basicFont.render(str(comhealth), True, BLACK)
    playerhealthtext = secondFont.render(str(playerhealth), True, BLACK)
    windowSurface.blit(background, (0,0))
    windowSurface.blit(comhealthtext, (555,89))
    damagetext = damageFont.render(str(heal), True, GREEN)
    windowSurface.blit(damagetext, (415,180))
    windowSurface.blit(playerhealthtext, (140,160))
    global player
    windowSurface.blit(player, (81,229))
    pygame.display.update()
    pygame.time.delay(1000)
    main()
    
def comattack():
    global comhealth
    global playerhealth
    global turn
    if comhealth > 100:
        comhealth = 100
    if playerhealth > 100:
        playerhealth = 100
    print "\nComputer attacks!"
    if comhealth <= 0:
        while True:
            windowSurface.blit(winscreen, (0,0))
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
    if playerhealth <= 18:
        cmain()

    elif comhealth <= 18:
        cheal()
        
    elif comhealth < 35:
        chance = randint(1,4)
        if chance == 1:
            cmain()
        elif chance == 2:
            cspec()
        elif chance == 3 or chance == 4:
            cheal()
        
    else:
        chance = randint(1,3)
        if chance == 1:
            cmain()
        elif chance == 2:
            cspec()
        elif chance == 3:
            cheal()
def main():
    global attack
    global pointer
    global background
    global pointerlocation
    global musicPlaying
    global basicFont
    global secondFont
    global BLACK
    global windowSurface
    global turn
    global comhealth
    global playerhealth
    global winscreen
    global losescreen
    global player
    while True:
        if attack == 1:
            pointerlocation = [530,361]
        elif attack == 2:
            pointerlocation = [530,386]
        elif attack == 3:
            pointerlocation = [530, 411]
        windowSurface.blit(background, (0,0))
        comhealthtext = basicFont.render(str(comhealth), True, BLACK)
        windowSurface.blit(comhealthtext, (555,89))
        playerhealthtext = secondFont.render(str(playerhealth),True, BLACK)
        windowSurface.blit(playerhealthtext,(140,160))
        windowSurface.blit(pointer,pointerlocation)
        windowSurface.blit(player, (81,229))
        pygame.display.update()
        if comhealth > 100:
            comhealth = 100
        if playerhealth > 100:
            playerhealth = 100
        if playerhealth <= 0:
            while True:
                windowSurface.blit(losescreen, (0,0))
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == QUIT:
                        pygame.quit()
                        sys.exit()
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_DOWN:
                    if attack < 3:
                        attack += 1
                if event.key == K_UP:
                    if attack > 1:
                        attack -= 1
                if event.key == K_RETURN:
                    if attack == 1:
                        pmain()
                    if attack == 2:
                        pspec()
                    if attack == 3:
                        pheal()

def choose():
    global windowSurface
    global playeronefront
    global playertwofront
    global playerthreefront
    global playerone
    global playertwo
    global playerthree
    global damageFont
    global player
    windowSurface.fill((250,137,250))
    playeronef = pygame.transform.scale(playeronefront, (100,100))
    windowSurface.blit(playeronef, (150,150))
    playertwof = pygame.transform.scale(playertwofront, (100,100))
    windowSurface.blit(playertwof, (325, 150))
    playerthreef = pygame.transform.scale(playerthreefront, (150,100))
    windowSurface.blit(playerthreef, (500,150))
    onetext = damageFont.render('1', True, BLACK)
    twotext = damageFont.render('2', True, BLACK)
    threetext = damageFont.render('3', True, BLACK)
    windowSurface.blit(onetext, (195, 260))
    windowSurface.blit(twotext, (360, 260))
    windowSurface.blit(threetext, (545, 260))
    pygame.display.update()
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN:
                if event.key == K_1:
                    player = playerone
                    pygame.mixer.music.load('MusicBattle.mp3')
                    pygame.mixer.music.play(-1, 0.0)
                    main()
                if event.key == K_2:
                    player = playertwo
                    pygame.mixer.music.load('MusicBattle.mp3')
                    pygame.mixer.music.play(-1, 0.0)
                    main()
                if event.key == K_3:
                    player = playerthree
                    pygame.mixer.music.load('MusicBattle.mp3')
                    pygame.mixer.music.play(-1, 0.0)
                    main()
                
choose()
