from random import randint
import sys
comhealth = 100
playerhealth = 100
turn = 0
def end():
    print "Game over!"
    sys.exit()
def main():
    global turn
    global comhealth
    global playerhealth
    if comhealth > 100:
        comhealth = 100
    if playerhealth > 100:
        playerhealth = 100
    if comhealth < 0:
        comhealth = 0
    if playerhealth < 0:
        playerhealth = 0
        
    print "\nComputer has", comhealth, "health."
    print "Player has", playerhealth, "health."
    
    if playerhealth == 0:
        print "Computer wins!"
        end()
        
    if turn == 0:
        playerattack()
    if turn == 1:
        comattack()

def cmain():
    global playerhealth
    damage = randint(18,25)
    playerhealth -= damage
    print "Computer uses it's main attack for", damage, "damage!"
    main()

def cspec():
    global playerhealth
    damage = randint(10,35)
    playerhealth -= damage
    print "Computer uses it's special attack for", damage, "damage!"
    main()

def cheal():
    global comhealth
    heal = randint(18,25)
    comhealth += heal
    print "Computer heals", heal, "health!"
    main()

def pmain():
    global comhealth
    damage = randint(18,25)
    comhealth -= damage
    print "Player uses it's main attack for", damage, "damage!"
    main()

def pspec():
    global comhealth
    damage = randint(10,35)
    comhealth -= damage
    print "Player uses it's special attack for", damage, "damage!"
    main()
    
def pheal():
    global playerhealth
    heal = randint(18,25)
    playerhealth += heal
    print "Player heals", heal, "health!"
    main()
    
def comattack():
    global comhealth
    global turn
    if comhealth == 0:
        print "Player wins!"
        end()
    turn = 0
    print "\nComputer attacks!"
    if comhealth > 35:
        chance = randint(1,3)
        if chance == 1:
            cmain()
        elif chance == 2:
            cspec()
        else:
            cheal()
    else:
        chance = randint(1,4)
        if chance == 1:
            cmain()
        if chance == 2:
            cspec()
        if chance == 3 or chance == 4:
            cheal()
def playerattack():
    global turn
    turn = 1
    print """
1: Main attack
2: Special attack
3: Heal"""
    while True:
        while True:
            try:
                choice = input()
                break
            except (EOFError, NameError, SyntaxError):
                print "Please enter a valid number!"
        if choice == 1:
            pmain()
        elif choice == 2:
            pspec()
        elif choice == 3:
            pheal()
        else:
            print "Please input 1, 2, or 3!"

main()
