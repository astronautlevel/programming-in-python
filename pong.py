#Tells it NOT to run the CMD friendly command
#This is an annotation
wait = False
#Makes it so the ball doesn't bounce immediatly starting the game
first_round = 100
#Makes pygame work
import pygame
import random
# Define some colors
black    = (   0,   0,   0)
white    = ( 255, 255, 255)
green    = (   0, 255,   0)
red      = ( 255,   0,   0)
blue     = (   0,   0, 255)
 
pygame.init()
  
# Set the width and height of the screen [width,height]
size=[800,400]
screen=pygame.display.set_mode(size)
 
pygame.display.set_caption("Pong")
 
#Loop until the user clicks the close button.
done=False
 
# Used to manage how fast the screen updates
clock=pygame.time.Clock()

# Sets the paddle's speed and position
x_speed = 0
rect_x = 400
rect_y = 350
# Sets the ball's starting position and speed
bx_speed = 1.25
by_speed = 1.25
ball_x = 375
ball_y = 175
# This class represents the paddle        
# It derives from the "Sprite" class in Pygame
class Block(pygame.sprite.Sprite):
     
    # Constructor. Pass in the color of the block, 
    # and its x and y position
    def __init__(self, color, width, height):
        # Call the parent class (Sprite) constructor
        pygame.sprite.Sprite.__init__(self) 
 
        # Create an image of the block, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([width, height])
        self.image.fill(color)

        # Fetch the rectangle object that has the dimensions of the image
        # image.
        # Update the position of this object by setting the values 
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
#Defines the paddle
paddle = pygame.sprite.RenderPlain()
player = Block(green,100,10)
paddle.add(player)

class Sphere(pygame.sprite.Sprite):
     
    # Constructor. Pass in the color of the block, 
    # and its x and y position
    def __init__(self, color, width, height):
        # Call the parent class (Sprite) constructor
        pygame.sprite.Sprite.__init__(self) 
 
        # Create an image of the block, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([width, height])
        self.image.fill(white)
        self.image.set_colorkey(white)
        pygame.draw.ellipse(self.image,color,[0,0,width,height])
 
        # Fetch the rectangle object that has the dimensions of the image
        # image.
        # Update the position of this object by setting the values 
        # of rect.x and rect.y
        self.rect = self.image.get_rect()
#Defines Ball
ball = Sphere(green, 25,25)
sphere = pygame.sprite.RenderPlain()
sphere.add(ball)
#Sets collision to False
blocks_hit_list = 0

#Resets the score
score = 0

#Import sound
# -------- Main Program Loop ----------
start = False
while start==False:
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            start = True
        elif event.type == pygame.QUIT:
            pygame.quit ()
    screen.fill(blue)
    pygame.draw.circle(screen, green, [375,175], 12, 0)
    pygame.draw.rect(screen, green, (400,350,100,10),0) 
    pygame.display.flip()
while done==False:
    # ALL EVENT PROCESSING SHOULD GO BELOW THIS COMMENT
    for event in pygame.event.get(): # User did something   
        if event.type == pygame.QUIT:
            print "QUITTER!"
            wait=True
            done=True
         
    # User pressed down on a key
        if event.type == pygame.KEYDOWN:
            # Figure out if it was an arrow key. If so
            # adjust speed.
            if event.key == pygame.K_LEFT:
                x_speed=-2.5
            if event.key == pygame.K_RIGHT:
                x_speed=2.5
             
    # User let up on a key
        if event.type == pygame.KEYUP:
            # If it is an arrow key, reset vector back to zero
            if event.key == pygame.K_LEFT:
                x_speed=0
            if event.key == pygame.K_RIGHT:
                x_speed=0
    # ALL EVENT PROCESSING SHOULD GO ABOVE THIS COMMENT
  
  
    # ALL GAME LOGIC SHOULD GO BELOW THIS COMMENT
    #Checks to see if the ball has collided with the paddle,
    #if so, bounce is true
    if pygame.sprite.spritecollide(player, sphere, False) and first_round <= 0:
          blocks_hit_list = 1   #Says "THE BALL HIT THE PADDLE!"
          clock.tick(100000)
            #Play bounce sound
     
    # Check to see if the ball hit the paddle.
    if blocks_hit_list == 1:
          by_speed = -1.25  #Makes the ball bounce
          blocks_hit_list = 0   #Makes it so the ball cannot bounce again
          score += 1
          first_round = 100

    if ball_x == 0 or ball_x == 775:   #Checks if the ball hit the wall
        bx_speed *= -1 #Bounce
        clock.tick(100000)
         #Play sound
    if ball_y == 0:
        by_speed *= -1   #Bounce
        clock.tick(100000)
         #Play sound
    #Did the ball pass the paddle?
    if ball_y >= 400:
        print"Game over!"
        break
    # ALL GAME LOGIC SHOULD GO ABOVE THIS COMMENT
 
     
 
    # ALL CODE TO DRAW SHOULD GO BELOW THIS COMMENT
     
    # First, clear the screen to any color. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill(blue)
    # Draw the paddle

    
    # Draw the ball
    ball.rect.x = ball_x
    ball.rect.y = ball_y
    sphere.draw(screen)
    # Move the rectangle
    rect_x += x_speed
    # Move the ball
    ball_x += bx_speed
    ball_y += by_speed
    # Draw the rectangle
    player.rect.x = rect_x
    player.rect.y = rect_y
    paddle.draw(screen)
    # Says GAME OVER

        
        
    # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
     
    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
 
    # FPS(Frames per second)
    clock.tick(240)
    #Makes the first round false so the ball bounces
    first_round = first_round - 1
     
# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit ()
print"Your score was:",score
raw_input("press enter or return to close")
