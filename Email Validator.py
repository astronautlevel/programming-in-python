import sys
import re

user_email = str(input("What is your email? "))
p = re.compile('^[A-Za-z0-9._%+]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')
if p.match(user_email):
    print("Valid email!")
else:
    print("Why do you hate valid emails?")
