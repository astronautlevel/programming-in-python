#This is a comment
morseDict = {
'A': '.-',
'B': '-...',
'C': '-.-.',
'D': '-..',
'E': '.',
'F': '..-.',
'G': '--.',
'H': '....',
'I': '..',
'J': '.---',
'K': '-.-',
'L': '.-..',
'M': '--',
'N': '-.',
'O': '---',
'P': '.--.',
'Q': '--.-',
'R': '.-.',
'S': '...',
'T': '-',
'U': '..-',
'V': '...-',
'W': '.--',
'X': '-..-',
'Y': '-.--',
'Z': '--..'
}

morse = ""
user_text = raw_input("Text to be converted to morse: ")
user_text = user_text.upper()
for i in user_text:
    if i != " ":
        morse += morseDict[i]
        morse += " "
    else:
        morse += "/ "
print morse
